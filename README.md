﻿#4 v vrsto #

### Opis igre ###

Preprosta igra za dva igralca, ki izmenično igrata poteze v polje s sedmimi stolpci in šestimi vrsticami. Igralec izbere stolpec v katerega bo igral in zasede najnižje še nezasedeno polje v tem stolpcu. Zmaga igralec, ki postavi štiri žetone v vrsto bodisi po diagonali, vrstici ali stolpcu. Če je polje napolnjeno in nobeden od igralcev ni zmagal, je igra neodločena.

### O repozitoriju ###

Repozitorij '4vvrsto' vsebuje projekt pri predmetu Programiranje 2 (študijsko leto 2014/2015). Napisan je v programskem jeziku Python 3.4 z uporabo modula Tkinter.

### Vsebina repozitorija ###

* `stiriLogika.py`: vsebuje logiko programa.

* `stiriGUI.py`: vsebuje grafični vmesnik, ki skrbi za prikaz podatkov in interakcijo z uporabnikom