__author__ = 'KukuljanT13'
import random

### Polje je organizirano: [[stolpec1], [stolpec2], ...]
### self.polje je seznam seznamov z 0, 'x' ali 'o'
### self.poteza je število igranih potez, da se ve kdo je na vrsti
### self.zadnja je seznam vseh potez (številke stolpcev)
### self.seIgra je merilec če je že kdo zmagal ali ne

class Igra():
    def __init__(self, polje=None, zadnja=None):
        self.polje = [[0 for x in range(6)] for y in range(7)]
        self.poteza = 0
        self.zadnja = []
        self.seIgra = True
        self.prveMoznePoteze = [0]*7     ## evidenca po stolpcih

    def preveriZmagovalca(self):         ### je v GUI-u
        a = zmagovalec(self.polje)
        if a:
            pass             ### zafrkne minmax metodo
            # self.seIgra = False
            # print("Zmagal je igralec",a)

    def igraj(self, stolpec):
        if not self.seIgra:
            return
        stolpec -= 1
        if self.prveMoznePoteze[stolpec] == 6:
            return

        if not 0 in self.polje[stolpec]:
            return
        self.zadnja.append(stolpec+1)
        self.polje[stolpec][self.prveMoznePoteze[stolpec]] = ('x' if self.poteza%2==0 else 'o')
        self.poteza += 1
        self.prveMoznePoteze[stolpec] += 1

    def razveljavi(self):
        if self.zadnja == [] or self.poteza == 0 or not self.seIgra:
            return
        self.polje[self.zadnja[-1]-1][self.prveMoznePoteze[self.zadnja[-1]-1]-1] = 0
        self.prveMoznePoteze[self.zadnja[-1]-1] -= 1
        self.zadnja = self.zadnja[:-1]
        self.poteza -= 1

    def moznePoteze(self):            ## vrne seznam možnih potez, v vrstnem redu za minmax - [4,5,3,6,2,7,1]
        vrsta = [4,5,3,6,2,7,1]
        vrni = []
        for x in range(3):              ## malo premeša seznam, da ne računalnik ne igra vedno enako.
            nakljucno = random.randint(0,1)
            if nakljucno == 1:
                vrsta[2*x+1], vrsta[2*x+2] = vrsta[2*x+2], vrsta[2*x+1]
        for c in vrsta:
            if self.polje[c-1][-1] == 0:
                vrni.append(c)
        return vrni

    def __str__(self):
        vrni = ""
        for x in range(6):
            for y in range(7):
                if self.polje[y][5-x]==0:
                    vrni += "   "
                else:
                    vrni += " " + self.polje[y][5-x] + " "
            vrni += "\n"
        return vrni

cetverke = []
#### stolpci
for j in range(7):
    for i in range(3):
        cetverke.append([(j, i+c) for c in range(4)])
#### vrstice
for i in range(6):
    for j in range(4):
        cetverke.append([(j+c, i) for c in range(4)])
#### diagonale desno gor
for i in range(3):
    for j in range(4):
        cetverke.append([(j+c, i+c) for c in range(4)])
#### diagonale desno dol
for i in range(3,6):
    for j in range(4):
        cetverke.append([(j+c, i-c) for c in range(4)])

def zmagovalec(polje):
    for stolpec in polje:   ## preveri po stolpcih
        for x in (2,1,0):
            if stolpec[x] != 0 and stolpec[x] == stolpec[x+1] == stolpec[x+2] == stolpec[x+3]:
                return stolpec[x]
    for i in range(6):    ## preveri po vrsticah             i - vrstični indeks, j - stolpični
        for j in range(4):
            if polje[j][i] != 0 and polje[j][i] == polje[j+1][i] == polje[j+2][i] == polje[j+3][i]:
                return polje[j][i]
    for i in range(3):    ## preveri po diagonalah desno gor
        for j in range(4):
            if polje[j][i] != 0 and polje[j][i] == polje[j+1][i+1] == polje[j+2][i+2] == polje[j+3][i+3]:
                return polje[j][i]
    for i in range(3,6):    ## preveri po diagonalah desno dol
        for j in range(4):
            if polje[j][i] != 0 and polje[j][i] == polje[j+1][i-1] == polje[j+2][i-2] == polje[j+3][i-3]:
                return polje[j][i]

def zmagovalecKoordinate(polje):         ## za izris črte skozi zmagovalne žetone
    for stStolpca, stolpec in enumerate(polje):   ## preveri po stolpcih
        for x in (2,1,0):
            if stolpec[x] != 0 and stolpec[x] == stolpec[x+1] == stolpec[x+2] == stolpec[x+3]:
                return [(stStolpca, x), (stStolpca, x + 3)]
    for i in range(6):    ## preveri po vrsticah             i - vrstični indeks, j - stolpični
        for j in range(4):
            if polje[j][i] != 0 and polje[j][i] == polje[j+1][i] == polje[j+2][i] == polje[j+3][i]:
                return [(j, i), (j+3, i)]
    for i in range(3):    ## preveri po diagonalah desno gor
        for j in range(4):
            if polje[j][i] != 0 and polje[j][i] == polje[j+1][i+1] == polje[j+2][i+2] == polje[j+3][i+3]:
                return [(j, i), (j+3, i+3)]
    for i in range(3,6):    ## preveri po diagonalah desno dol
        for j in range(4):
            if polje[j][i] != 0 and polje[j][i] == polje[j+1][i-1] == polje[j+2][i-2] == polje[j+3][i-3]:
                return [(j, i), (j+3, i-3)]

def hevristika(polje, prveMoznePoteze, poteza, printamo = False):
    """
    Vrednosti: 4/4 - 100
               3+3 - 90 (skoraj zagotova zmaga)
               3/4 - 50/10 odvisno od sodosti/lihosti grožnje, ki jo ustvarja
               2/4 - 5
    :param polje: delno igrano polje, ki ga funkcija ocenjuje
    :return: ocena polja
    """
    vrni = 0

    groznjeX = []  ### seznama za dodajanje polj, kjer manjka 4ti žeton  ...  najprej stolpec, potem vrstica
    groznjeO = []
    groznjePoStolpcih = [[] for x in range(7)]  ## seznam seznamov za hevristično ocenjevanje
    ##### išče 3-vrstne grožnje #####  in po dva skupaj #####
    for stStolpca, stolpec in enumerate(polje):   ## preveri po stolpcih   -- tu dva zapored štejeta 5
        for x in range(0,3):
            a = stolpec[x:x+4]
            if a[0] == 0:
                break
            if a.count('o')==4:
                return -100
            if a.count('x')==4:
                return 100
            if a.count(0)==2:
                if 'x' not in a:
                    vrni -= 2*(2 if stStolpca +1 in [3, 4, 5] else 1)
                if 'o' not in a:
                    vrni += 2*(2 if stStolpca +1 in [3, 4, 5] else 1)
            if a.count(0) != 1 or a[3]!= 0:
                continue
            if 'o' not in a:
                groznjeX.append((stStolpca, x+3))
                groznjePoStolpcih[stStolpca].append((x+3,'x'))
            if 'x' not in a:
                groznjeO.append((stStolpca, x+3))
                groznjePoStolpcih[stStolpca].append((x+3,'o'))
    for i in range(6):    ## preveri po vrsticah             i - vrstični indeks, j - stolpični              2jka šteje 10
        for j in range(4):
            a = []
            a.append(polje[j][i])
            a.append(polje[j+1][i])
            a.append(polje[j+2][i])
            a.append(polje[j+3][i])
            if a.count('o')==4:
                return -100
            if a.count('x')==4:
                return 100
            if a.count(0)==2:
                if 'x' not in a:
                    vrni -= 2*(1.5 if i  in [1, 3, 4, 5] else 1)
                if 'o' not in a:
                    vrni += 2*(2 if i +1 in [1, 3, 4, 5] else 0)
            if a.count(0)!=1:
                continue
            for c in range(4):
                if polje[j+c][i] == 0:
                    if 'o' not in a:
                        groznjeX.append((j+c,i))
                        groznjePoStolpcih[j+c].append((i,'x'))
                    elif 'x' not in a:
                        groznjeO.append((j+c,i))
                        groznjePoStolpcih[j+c].append((i,'o'))
                    break

    for i in range(3):    ## preveri po diagonalah desno gor
        for j in range(4):
            a = []
            a.append(polje[j][i])
            a.append(polje[j+1][i+1])
            a.append(polje[j+2][i+2])
            a.append(polje[j+3][i+3])
            if a.count('o')==4:
                return -100
            if a.count('x')==4:
                return 100
            if a.count(0)==2:
                if 'x' not in a:
                    vrni -= 3
                if 'o' not in a:
                    vrni += 3
            if a.count(0)!=1:
                continue
            for c in range(4):
                if polje[j+c][i+c] == 0:
                    if 'o' not in a:
                        groznjeX.append((j+c,i+c))
                        groznjePoStolpcih[j+c].append((i+c,'x'))
                    elif 'x' not in a:
                        groznjeO.append((j+c,i+c))
                        groznjePoStolpcih[j+c].append((i+c,'o'))
                    break

    for i in range(3,6):    ## preveri po diagonalah desno dol
        for j in range(4):
            a = []
            a.append(polje[j][i])
            a.append(polje[j+1][i-1])
            a.append(polje[j+2][i-2])
            a.append(polje[j+3][i-3])
            if a.count('o')==4:
                return -100
            if a.count('x')==4:
                return 100
            if a.count(0)==2:
                if 'x' not in a:
                    vrni -= 3
                if 'o' not in a:
                    vrni += 3
            if a.count(0)!=1:
                continue
            for c in range(4):
                if polje[j+c][i-c] == 0:
                    if 'o' not in a:
                        groznjeX.append((j+c, i-c))
                        groznjePoStolpcih[j+c].append((i-c,'x'))
                    elif 'x' not in a:
                        groznjeO.append((j+c, i-c))
                        groznjePoStolpcih[j+c].append((i-c,'o'))
                    break

    for x in polje[3]:
        if x=='x':
            vrni+=5
        if x=='o':
            vrni-=5

    trenutneGroznje = {'x':0, 'o':0}
    for i,x in enumerate(groznjePoStolpcih):
        for visina, igralec in x:
            if prveMoznePoteze[i] == visina:
                trenutneGroznje[igralec] += 1
    if poteza % 2 == 0:
        if trenutneGroznje['x'] != 0:
            return 100
        if trenutneGroznje['o'] > 1:
            return -100
    else:
        if trenutneGroznje['o'] != 0:
            return -100
        if trenutneGroznje['x'] > 1:
            return 100

    oImaNaSodi = 0
    oImaNaLihi = 0
    xImaNaSodi = 0
    xImaNaLihi = 0           ## skoraj nepomembna
    xImaPrisilo = None                    ### prisila sta 2 grožnji istega igralca zapovrstjo
    oImaPrisilo = None
    bonus = [None for x in range(7)]         ## dodatna vrednost za x in o za najnižje možne grožnje, ki koristijo
    for stolpecGroznje, x in enumerate(groznjePoStolpcih):
        x.sort()
        if len(x)==1 and x[0][0] == prveMoznePoteze[stolpecGroznje]:
            if x[0][1]=='o':
                vrni -= 10
            else:
                vrni += 10
        if x == []:
            continue
        prva, okupator = x[0]
        if len(x)>1:                           ### odstrani neuporabne grožnje - grožnje ki so točno nad grožnjo nasprotnega igralca
            for c in range(len(x)-1, 0, -1):
                potencialniNeuporabnez = x[c]
                for visina, igralec in x:
                    if visina == potencialniNeuporabnez[0]-1 and igralec != potencialniNeuporabnez[1]:
                        del x[c]
                        break

        if len(x) == 1:
            if okupator == 'x' and prva % 2 == 0:
                if prva == 2:
                    bonus[stolpecGroznje] = 'x'
                xImaNaSodi += 1
            if okupator == 'o' and prva % 2 == 1:
                if prva == 1:
                    bonus[stolpecGroznje] = 'o'
                oImaNaLihi += 1
            if okupator == 'o' and prva % 2 == 0:
                oImaNaSodi += 1
            if okupator == 'x' and prva % 2 == 1:
                xImaNaLihi += 1

        if len(x)>1:
            prisila = False
            for c in range(len(x)-1):
                rezina = []
                prva = x[c]
                rezina.append(prva)
                druga = x[c+1]
                rezina.append(druga)
                if rezina[0][0]+1 == rezina[1][0] and rezina[0][1]==rezina[1][1] and okupator == rezina[0][1]:
                    if okupator == 'x':
                        if xImaPrisilo == None:
                            xImaPrisilo = rezina[0][0] - prveMoznePoteze[stolpecGroznje]
                        elif xImaPrisilo > rezina[0][0] - prveMoznePoteze[stolpecGroznje]:
                            xImaPrisilo = rezina[0][0] - prveMoznePoteze[stolpecGroznje]
                    if okupator == 'o':
                        if oImaPrisilo == None:
                            oImaPrisilo = rezina[0][0] - prveMoznePoteze[stolpecGroznje]
                        elif oImaPrisilo > rezina[0][0] - prveMoznePoteze[stolpecGroznje]:
                            oImaPrisilo = rezina[0][0] - prveMoznePoteze[stolpecGroznje]
                    prisila = True
                    break
            if prisila:
                continue
            smoZePreverili = False
            prvaVisina = 7
            okupator = None
            for i, (visina, igralec) in enumerate(x):
                if smoZePreverili:          ## preveri, če je trenutna višina enaka neki prejšnji (točno prejšnji) - potem smo že preverili sproti v prejšnjem krogu zanke
                    smoZePreverili = False
                    continue
                if i<len(x)-1:                ## če to ni zadnja grožnja v stolpcu
                    naslednji = x[i+1]
                else:
                    naslednji = (None, None)
                if naslednji[0] == visina:
                    smoZePreverili = True
                if visina<prvaVisina:       ## se izvede točno v prvem krogu zanke
                    prvaVisina = visina
                    okupator = igralec
                    if visina % 2 == 0:
                        if smoZePreverili:
                            if visina == 2:
                                bonus[stolpecGroznje] = 'x'
                            oImaNaLihi += 1
                            xImaNaLihi += 1
                        else:
                            if okupator == 'o':
                                oImaNaLihi += 1
                            else:
                                if visina == 2:
                                    bonus[stolpecGroznje] = 'x'
                                xImaNaLihi += 1
                    else:
                        if smoZePreverili:
                            if visina == 1:
                                bonus[stolpecGroznje] = 'o'
                            oImaNaSodi += 1
                            xImaNaSodi += 1
                        else:
                            if okupator == 'o':
                                if visina == 1:
                                    bonus[stolpecGroznje] = 'o'
                                oImaNaSodi += 1
                            else:
                                xImaNaSodi += 1
                else:
                    zeIma = False
                    for vi, ig in x:            ### preveri če ima igralec že grožnjo istega predznaka v tem stolpcu - take ni treba šteti.
                        if vi == visina:
                            break
                        if vi%2 == visina%2 and ig == igralec:
                            zeIma = True
                            break
                    if zeIma:
                        continue
                    if visina%2 == 1:
                        if igralec == 'o':
                            oImaNaLihi += 1
                        else:
                            xImaNaLihi += 1
                    else:
                        if igralec == 'o' and not (okupator == 'o' and prvaVisina%2 == 0): ########pozor z if-om!!!!!!!
                            oImaNaSodi += 1
                        elif okupator == 'x':   ## če ima 'o' grožnjo pred 'x' na lihem mestu mu ne koristi (oba pogoja skupaj)
                            xImaNaSodi += 1
                        elif prvaVisina%2 == 0:
                            xImaNaSodi += 1
    ### grožnje in prisile so preštete
    if xImaPrisilo and oImaPrisilo:              ## kdaj se že pozna zmagovalca
        if oImaPrisilo<xImaPrisilo:
            return -100
        elif oImaPrisilo>xImaPrisilo:
            return 100
        else:
            if poteza%2 == 0:
                return 100
            else:
                return -100
    if xImaPrisilo != None:
        return 100
    if oImaPrisilo != None:
        return -100
    for bon in bonus:
        if bon == 'x':
            vrni += 10
        if bon == 'o':
            vrni -= 10

    if printamo:
        print("bonus:", bonus, "x ima sode:", xImaNaSodi, "lihi:",xImaNaLihi, "o ima sode:", oImaNaSodi, "lihe", oImaNaLihi)

    vrni += xImaNaLihi*3
    vrni += xImaNaSodi*30
    vrni -= oImaNaSodi*10
    vrni -= oImaNaLihi*20
    if vrni >= 100:
        return 95
    if vrni <= -100:
        return -95

    return vrni

def alfaBetaPruning(igra, alfa=-101, beta=101, globina=3):
    if igra.poteza == 41:          ### zaustavitveni pogoji za 'x' in 'o'
        return hevristika(igra.polje, igra.prveMoznePoteze, igra.poteza), igra.moznePoteze()[0]
    if igra.poteza == 42:
        return hevristika(igra.polje, igra.prveMoznePoteze, igra.poteza), None
    if len(igra.moznePoteze())==0:
        return hevristika(igra.polje, igra.prveMoznePoteze, igra.poteza), None
    if globina == 0:
        return hevristika(igra.polje, igra.prveMoznePoteze, igra.poteza), None

    ############ igralec X
    if igra.poteza % 2 == 0:
        moznePoteze = igra.moznePoteze()
        mojaNajboljsa = 0
        mojPricakovan = -101
        for poteza in moznePoteze:
            igra.igraj(poteza)
            zmago = zmagovalec(igra.polje)
            if zmago == 'x':  ## če je poteza zmagovalna, hočemo da takoj pove, da tukaj zmaga in ni treba preverjat ostalih potez
                mojaNajboljsa = poteza
                mojPricakovan = 100
                igra.razveljavi()
                return mojPricakovan, mojaNajboljsa
            sovragovaNajboljsa = 0               ## vezano na potezo
            sovragovPotencial = 101           ## vezano na vrednost - hevristiko
            for sovragova in igra.moznePoteze():
                igra.igraj(sovragova)
                zmago = zmagovalec(igra.polje)
                if zmago == 'o':    ## če je poteza zmagovalna za 'o', nočemo, gre globje v drevo
                    hevr = -100
                    sovragovaNajboljsa = sovragova
                else:
                    hevr, p = alfaBetaPruning(igra, alfa, beta, globina - 1)
                igra.razveljavi()
                if hevr <= alfa:
                    break
                if hevr < sovragovPotencial:
                    sovragovaNajboljsa = sovragova
                    sovragovPotencial = hevr
            igra.razveljavi()
            if hevr <= alfa:   ## se bo izvedlo samo če bo break v sovragovi for zanki
                continue
            if sovragovPotencial > alfa:
                alfa = sovragovPotencial
            if mojPricakovan < sovragovPotencial:
                mojaNajboljsa = poteza
                mojPricakovan = sovragovPotencial
        return mojPricakovan, mojaNajboljsa

    ######## igralec O
    if igra.poteza % 2 == 1:
        moznePoteze = igra.moznePoteze()
        mojaNajboljsa = 0
        mojPricakovan = 101
        for poteza in moznePoteze:
            igra.igraj(poteza)
            zmago = zmagovalec(igra.polje)
            if zmago == 'o':  ## če je poteza zmagovalna, hočemo da takoj pove, da tukaj zmaga in ni treba preverjat ostalih potez
                mojaNajboljsa = poteza
                mojPricakovan = -100
                igra.razveljavi()
                return mojPricakovan, mojaNajboljsa
            sovragovaNajboljsa = 0               ## vezano na potezo
            sovragovPotencial = -101           ## vezano na vrednost - hevristiko
            if igra.moznePoteze()==[]:
                zmago = zmagovalec(igra.polje)
                igra.razveljavi()
                if not zmago:
                    return 0, None
                if zmago == 'o':
                    return -100, None
                return 100, None
            for sovragova in igra.moznePoteze():
                igra.igraj(sovragova)
                zmago = zmagovalec(igra.polje)
                if zmago == 'x':    ## če je poteza zmagovalna za 'x', nočemo, gre globje v drevo
                    hevr = 100
                    sovragovaNajboljsa = sovragova
                else:
                    hevr, p = alfaBetaPruning(igra, alfa, beta, globina - 1)
                igra.razveljavi()
                if hevr >= beta:
                    break
                if hevr > sovragovPotencial:
                    sovragovaNajboljsa = sovragova
                    sovragovPotencial = hevr
            igra.razveljavi()
            if hevr >= beta:   ## se bo izvedlo samo če bo break v sovragovi for zanki
                continue
            if sovragovPotencial < beta:  ## že
                beta = sovragovPotencial
            if mojPricakovan > sovragovPotencial:
                mojaNajboljsa = poteza
                mojPricakovan = sovragovPotencial
        return mojPricakovan, mojaNajboljsa


############# TEST ###################


a = Igra()
# i = -1
# while True:
#     if a.moznePoteze()==[]:
#         print("neodločeno")
#         break
#     i+=1
#     if i%2 == 1:
#         c = alfaBetaPruning(a)
#         pot = c[1]
#         a.igraj(pot)
#         print(a)
#         if zmagovalec(a.polje):
#             break
#         print(c[0])
#         continue
#     c = input("Igraj potezo ")
#     a.igraj(int(c))
#     print(a)
#     if zmagovalec(a.polje):
#         break
#     if not a.seIgra:
#         break
# print(a.zadnja)
#
#
#
# a.igraj(4)
# a.igraj(4)
# a.igraj(4)
# a.igraj(4)
# a.igraj(5)
# a.igraj(3)
# a.igraj(4)
# a.igraj(4)
# a.igraj(5)
# a.igraj(3)
# a.igraj(5)
# a.igraj(5)
# a.igraj(3)
# a.igraj(1)
# a.igraj(5)
# a.igraj(1)
# a.igraj(3)
# a.igraj(5)
# a.igraj(3)
# a.igraj(6)
# a.igraj(7)
# a.igraj(3)
#
# print(hevristika(a.polje))
#hevr, pot = alfaBetaPruning(a)
#print(hevr, pot)
# a.igraj(4)
# a.igraj(4)
# a.igraj(4)
# a.igraj(4)
# a.igraj(4)
# a.igraj(4)
# a.igraj(4)
# print(str(a))
