__author__ = 'Teo'


import tkinter as t
from stiriLogika import *
from time import clock


class Aplikacija():
    def __init__(self, master):
        self.barvaO = '#E60000'  ## temno rdeča
        self.barvaX = '#FFCC00'  ## motno rumena
        self.barvaOkvira = "#00008A"  ## temno modra
        self.sencenaBarvaOkvira = "#000066"  ## senčno temno modra

        self.velikostPolja = 100
        self.radijZetona = 35


        self.naPoteziJeClovek = False
        self.x = True           ### True - človek, False - računalnik
        self.o = True
        self.igra = None
        self.seIgra = False
        self.globinaO = 3
        self.globinaX = 3
        self.zmagovalec = None

        self.napis = t.StringVar(value="Izberite igralca in pričnite igro.")
        self.label = t.Label(master, textvariable = self.napis)
        self.label.grid(row=0, column=0)

        self.napisRumeni = t.StringVar(value="Rumeni:")
        self.labelRumeni = t.Label(master, textvariable = self.napisRumeni, bg=self.barvaX)
        self.labelRumeni.grid(row=0, column=1)
        self.napisRumeni2 = t.StringVar(value="človek.")
        self.labelRumeni2 = t.Label(master, textvariable = self.napisRumeni2)
        self.labelRumeni2.grid(row=0, column=2)

        self.napisRdeci = t.StringVar(value="Rdeči:")
        self.labelRdeci = t.Label(master, textvariable = self.napisRdeci, bg=self.barvaO)
        self.labelRdeci.grid(row=0, column=3)
        self.napisRdeci2 = t.StringVar(value="človek.")
        self.labelRdeci2 = t.Label(master, textvariable = self.napisRdeci2)
        self.labelRdeci2.grid(row=0, column=4)

        ####  Meniji  ####
        glavniMeni = t.Menu(master)
        master.config(menu=glavniMeni)

        novaMeni = t.Menu(glavniMeni)
        tezavnostMeni = t.Menu(glavniMeni)
        igraMeni = t.Menu(glavniMeni)
        rumeniMeni = t.Menu(glavniMeni)
        rdeciMeni = t.Menu(glavniMeni)

        glavniMeni.add_cascade(label="Nova igra", menu=novaMeni)
        novaMeni.add_command(label="Prični", command= lambda: self.novaIgra())

        glavniMeni.add_cascade(label="Igra", menu=igraMeni)
        igraMeni.add_command(label="Razveljavi  (desni miškin klik)", command= lambda: self.razveljavi())
        igraMeni.add_command(label="Nadaljuj  (dvojni levi klik)", command= lambda: self.nadaljuj())

        glavniMeni.add_cascade(label="Rumeni", menu=rumeniMeni)
        rumeniMeni.add_command(label="Človek", command= lambda: self.nastaviRumenega(clovek=True))
        rumeniMeni.add_command(label="Računalnik lahka", command= lambda: self.nastaviRumenega(clovek=False, globina=2))
        rumeniMeni.add_command(label="Računalnik srednja", command= lambda: self.nastaviRumenega(clovek=False, globina=3))
        rumeniMeni.add_command(label="Računalnik težka", command= lambda: self.nastaviRumenega(clovek=False, globina=4))

        glavniMeni.add_cascade(label="Rdeči", menu=rdeciMeni)
        rdeciMeni.add_command(label="Človek", command= lambda: self.nastaviRdecega(clovek=True))
        rdeciMeni.add_command(label="Računalnik lahka", command= lambda: self.nastaviRdecega(clovek=False, globina=2))
        rdeciMeni.add_command(label="Računalnik srednja", command= lambda: self.nastaviRdecega(clovek=False, globina=3))
        rdeciMeni.add_command(label="Računalnik težka", command= lambda: self.nastaviRdecega(clovek=False, globina=4))

        self.canvas = t.Canvas(master, width=7*self.velikostPolja, height=6*self.velikostPolja)
        self.canvas.create_rectangle(0, 0, 7*self.velikostPolja, 6*self.velikostPolja, fill=self.barvaOkvira)
        self.canvas.grid(row=1, column=0, rowspan=8, columnspan=10)
        self.narisiPraznaPolja()

        self.canvas.bind("<Button-1>", self.klik)
        self.canvas.bind("<Button-3>", self.razveljavi)
        self.canvas.bind("<Double-Button-1>", self.nadaljuj)        ## ne dela

    def narisiPraznaPolja(self):
        razlika = self.velikostPolja/2 - self.radijZetona
        faktor = 0.4          ## za velikost obroča okoli belega obroča
        for x in range(7):
            for y in range(6):
                self.canvas.create_oval(x*self.velikostPolja + razlika*faktor, y*self.velikostPolja + razlika*faktor, (x+1)*self.velikostPolja - razlika*faktor, (y+1)*self.velikostPolja - razlika*faktor, fill=self.sencenaBarvaOkvira)
                self.canvas.create_oval(x*self.velikostPolja + razlika, y*self.velikostPolja + razlika, (x+1)*self.velikostPolja - razlika, (y+1)*self.velikostPolja - razlika, fill="white")

    def nastaviRumenega(self, *args, **kwargs):
        if "globina" in kwargs.keys():
            self.globinaX = kwargs["globina"]
        if "clovek" in kwargs.keys():
            self.x = kwargs["clovek"]
        if self.x:
            self.napisRumeni2.set("človek.")
        else:
            self.napisRumeni2.set("računalnik " + ("lahka." if self.globinaX==2 else ("srednja." if self.globinaX==3 else "težka.")))

    def nastaviRdecega(self, *args, **kwargs):
        if "globina" in kwargs.keys():
            self.globinaO = kwargs["globina"]
        if "clovek" in kwargs.keys():
            self.o = kwargs["clovek"]
        if self.o:
            self.napisRdeci2.set("človek.")
        else:
            self.napisRdeci2.set("računalnik " + ("lahka." if self.globinaO==2 else ("srednja." if self.globinaO==3 else "težka.")))

    def novaIgra(self):
        self.igra = Igra()
        self.seIgra = True
        self.narisiIgro()
        self.igrajX()

    def igrajX(self):
        if not self.x and self.seIgra:
            self.napis.set("Rumeni razmišlja.")
            self.label.update()
            poteza = self.igrajRacunalnik()
            self.igra.igraj(poteza)
            self.narisiIgro()
            self.preveriZmagovalca()
            self.igrajO()
        elif self.seIgra:
            self.naPoteziJeClovek = True
            self.napis.set("Rumeni, igraj!")

    def igrajO(self):
        if not self.o and self.seIgra:
            self.napis.set("Rdeči razmišlja.")
            self.label.update()
            poteza = self.igrajRacunalnik()
            self.igra.igraj(poteza)
            self.narisiIgro()
            self.preveriZmagovalca()
            self.igrajX()
        elif self.seIgra:
            self.naPoteziJeClovek = True
            self.napis.set("Rdeči, igraj!")

    def igrajRacunalnik(self):
        a = clock()
        if self.igra.poteza % 2 == 0:
            globina = self.globinaX
        else:
            globina = self.globinaO
        hevr, poteza = alfaBetaPruning(self.igra, -101, 101, globina)
        return poteza

    def preveriZmagovalca(self):
        zmago = zmagovalec(self.igra.polje)
        if zmago:
            if zmago=='x':
                self.napis.set("Zmagal je rumeni!")
            if zmago=='o':
                self.napis.set("Zmagal je rdeči!")
            self.seIgra = False
            self.naPoteziJeClovek = False
            self.zmagovalec = zmago
            return
        if self.igra.moznePoteze()==[]:
            self.napis.set("Nedoločeno.")
            self.seIgra = False
            self.naPoteziJeClovek = False

    def klik(self, event):
        if self.seIgra and self.naPoteziJeClovek:
            stolpec = event.x // self.velikostPolja +1
            self.igra.igraj(stolpec)
            self.naPoteziJeClovek = False
            self.narisiIgro()
            self.preveriZmagovalca()
            if self.igra.poteza % 2 == 0:
                self.igrajX()
            else:
                self.igrajO()

    def razveljavi(self, event=None):
        if self.igra==None:
            return
        self.igra.razveljavi()
        self.narisiIgro()
        self.naPoteziJeClovek = False
        self.seIgra = False
        self.zmagovalec = None

    def nadaljuj(self, e=None):
        if self.igra==None or self.zmagovalec:
            return
        self.seIgra = True
        if self.igra.poteza % 2 == 0:
            self.igrajX()
        else:
            self.igrajO()

    def narisiIgro(self):
        self.canvas.delete(all)
        razlika = self.velikostPolja/2 - self.radijZetona
        self.canvas.create_rectangle(0, 0, 7*self.velikostPolja, 6*self.velikostPolja, fill=self.barvaOkvira)
        self.narisiPraznaPolja()
        for stStolpca, stolpec in enumerate(self.igra.polje):
            for visina, polje in enumerate(stolpec):
                if polje=='x':
                    self.canvas.create_oval(stStolpca*self.velikostPolja + razlika, 6*self.velikostPolja - (visina*self.velikostPolja + razlika), (stStolpca+1)*self.velikostPolja - razlika,
                                            6*self.velikostPolja -((visina+1)*self.velikostPolja - razlika), fill=self.barvaX)
                if polje == 'o':
                    self.canvas.create_oval(stStolpca*self.velikostPolja + razlika, 6*self.velikostPolja - (visina*self.velikostPolja + razlika), (stStolpca+1)*self.velikostPolja - razlika,
                                            6*self.velikostPolja -((visina+1)*self.velikostPolja - razlika), fill=self.barvaO)
        koordinate = zmagovalecKoordinate(self.igra.polje)
        if koordinate:
            [(x0, y0), (x1,y1)] = koordinate
            self.canvas.create_line((x0+1/2)*self.velikostPolja, 6*self.velikostPolja -(y0+1/2)*self.velikostPolja, (x1+1/2)*self.velikostPolja, 6*self.velikostPolja -(y1+1/2)*self.velikostPolja,width=4)
        self.canvas.update_idletasks()


masterYoda = t.Tk()
masterYoda.title('4 v vrsto')
app = Aplikacija(masterYoda)
masterYoda.mainloop()
